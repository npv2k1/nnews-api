import { HttpService } from '@nestjs/axios';
import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';

import { PrismaService } from 'nestjs-prisma';
import { map } from 'rxjs';

// import worker from "../worker/pullRss"

@Controller('playground')
export class PlaygroundController {
  constructor(
    private readonly prisma: PrismaService,
    private readonly httpService: HttpService
  ) {}

  @Get()
  async getAll() {
    const sources = await this.prisma.source.findMany();
    // sources.map(source=>{
    //   const dt = worker(source.url)
    // })r
    return sources;
  }

  @Get('demo')
  async cjsdc() {
    const res = await this.httpService.get('https://www.google.com/').pipe(map(res => res.data),);

    console.log(res);

    return 'Hello';
  }

  @Get('/:id')
  getOne(@Param('id') id: number) {
    return `Day la id: ${id}`;
  }

  @Post()
  create(@Body() body) {
    return body;
  }

  @Put('/:id')
  update(@Param('id') id: number, @Body() body) {
    return `Day la id: ${id} update ${body}`;
  }
}
