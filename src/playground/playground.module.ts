import {  Module } from '@nestjs/common';
import { PlaygroundService } from './playground.service';
import { PlaygroundResolver } from './playground.resolver';
import { PlaygroundController } from './playground.controller';
import { HttpModule, HttpService } from '@nestjs/axios';

@Module({
  imports: [HttpModule],
  controllers: [PlaygroundController],
  providers: [PlaygroundResolver, PlaygroundService],
})
export class PlaygroundModule {}
