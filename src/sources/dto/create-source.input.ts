import { InputType, Int, Field } from '@nestjs/graphql';
import { IsNotEmpty, IsUrl } from 'class-validator';

@InputType()
export class CreateSourceInput {
  @Field()
  @IsNotEmpty()
  name: string;
  
  @Field()
  @IsNotEmpty()
  @IsUrl()
  url: string;
}
