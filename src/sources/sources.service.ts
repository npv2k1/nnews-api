import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { PrismaService } from 'nestjs-prisma';
import { CreateSourceInput } from './dto/create-source.input';
import { UpdateSourceInput } from './dto/update-source.input';

@Injectable()
export class SourcesService {
  constructor(private prisma: PrismaService) {}
  create(createSourceInput: CreateSourceInput) {
    return this.prisma.source.create({
      data: createSourceInput,
    });
  }

  findAll() {
    return this.prisma.source.findMany();
  }

  findOne(id: number) {
    return `This action returns a #${id} source`;
  }

  update(id: number, updateSourceInput: UpdateSourceInput) {
    return `This action updates a #${id} source`;
  }

  remove(id: number) {
    return `This action removes a #${id} source`;
  }
}
