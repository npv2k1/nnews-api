import { ObjectType, Field, Int } from '@nestjs/graphql';

@ObjectType()
export class Source {
  @Field()
  name: string;
  @Field()
  url: string;
}
