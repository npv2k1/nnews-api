import { PrismaService } from 'nestjs-prisma';
import { Prisma, User } from '@prisma/client';
import {
  Injectable,
  NotFoundException,
  BadRequestException,
  ConflictException,
  UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { PasswordService } from './password.service';
import { SignupInput } from './dto/signup.input';
import { Token } from './models/token.model';
import { SecurityConfig } from 'src/common/configs/config.interface';
import * as crypto from 'crypto';
import { MailerService } from '@nestjs-modules/mailer';
import moment from 'moment';
import { ResetPasswordInput } from './dto/reset-password.input';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly prisma: PrismaService,
    private readonly passwordService: PasswordService,
    private readonly configService: ConfigService,
    private readonly mailerService: MailerService
  ) {}

  async createUser(payload: SignupInput): Promise<Token> {
    const hashedPassword = await this.passwordService.hashPassword(
      payload.password
    );

    try {
      const user = await this.prisma.user.create({
        data: {
          ...payload,
          password: hashedPassword,
          role: 'USER',
        },
      });

      return this.generateTokens({
        userId: user.id,
      });
    } catch (e) {
      if (
        e instanceof Prisma.PrismaClientKnownRequestError &&
        e.code === 'P2002'
      ) {
        throw new ConflictException(`Email ${payload.email} already used.`);
      } else {
        throw new Error(e);
      }
    }
  }

  async login(email: string, password: string): Promise<Token> {
    const user = await this.prisma.user.findUnique({ where: { email } });

    if (!user) {
      throw new NotFoundException(`No user found for email: ${email}`);
    }

    const passwordValid = await this.passwordService.validatePassword(
      password,
      user.password
    );

    if (!passwordValid) {
      throw new BadRequestException('Invalid password');
    }

    return this.generateTokens({
      userId: user.id,
    });
  }

  validateUser(userId: string): Promise<User> {
    return this.prisma.user.findUnique({ where: { id: userId } });
  }

  getUserFromToken(token: string): Promise<User> {
    const id = this.jwtService.decode(token)['userId'];
    return this.prisma.user.findUnique({ where: { id } });
  }

  generateTokens(payload: { userId: string }): Token {
    return {
      accessToken: this.generateAccessToken(payload),
      refreshToken: this.generateRefreshToken(payload),
    };
  }

  private generateAccessToken(payload: { userId: string }): string {
    let p = {
      ...payload,
      'https://hasura.io/jwt/claims': {
        'x-hasura-allowed-roles': ['admin', 'user'],
        'x-hasura-default-role': 'user',
        'x-hasura-user-id': payload.userId,
        'x-hasura-role': 'user',
      },
    };
    return this.jwtService.sign(p);
  }

  private generateRefreshToken(payload: { userId: string }): string {
    const securityConfig = this.configService.get<SecurityConfig>('security');
    return this.jwtService.sign(payload, {
      secret: this.configService.get('JWT_REFRESH_SECRET'),
      expiresIn: securityConfig.refreshIn,
    });
  }

  refreshToken(token: string) {
    try {
      const { userId } = this.jwtService.verify(token, {
        secret: this.configService.get('JWT_REFRESH_SECRET'),
      });

      return this.generateTokens({
        userId,
      });
    } catch (e) {
      throw new UnauthorizedException();
    }
  }

  private generateResetToken(user: User): string {
    // Date now - will use it for expiration
    const now = new Date();

    // Convert to Base64
    const timeBase64 = Buffer.from(now.toISOString()).toString('base64');

    //Convert to Base64 user UUID - will use for retrieve user
    const userUUIDBase64 = Buffer.from(user.id).toString('base64');

    // User info string - will use it for sign and use token once
    const userString = `${user.id}${user.email}${user.password}${user.updatedAt}`;
    const userStringHash = crypto
      .createHash('md5')
      .update(userString)
      .digest('hex');

    // Generate a formatted string [time]-[userSign]-[userUUID]
    const tokenize = `${timeBase64}-${userStringHash}-${userUUIDBase64}`;

    // encrypt token
    const key = crypto.createHash('sha256').update('popcorn').digest();

    const IV_LENGTH = 16;
    const iv = crypto.randomBytes(IV_LENGTH);
    const cipher = crypto.createCipheriv('aes-256-cbc', key, iv);
    const encrypted = cipher.update(tokenize);

    const result = Buffer.concat([encrypted, cipher.final()]);

    // formatted string [iv]:[token]
    return iv.toString('hex') + ':' + result.toString('hex');
  }
  private getUserUUIDFromToken(token: string): string {
    try {
      const userUUIDHash = token.split('-')[2];
      return Buffer.from(userUUIDHash, 'base64').toString('ascii');
    } catch (error) {
      console.log('getUserUUIDFromToken', error);
      return '';
    }
  }
  private validateResetToken(user: User, token: string): boolean {
    console.log('token', token);
    // Split token string and retrieve timeInfo and userInfoHash
    const [timeHBase64, reqUserStringHash] = token.split('-');

    console.log('validateResetToken', timeHBase64, reqUserStringHash);

    const timestamp = Buffer.from(timeHBase64, 'base64').toString('ascii');

    // Using moment.diff method for retrieve dates difference in hours
    const tokenTimestampDate = moment(timestamp);
    const now = moment();

    // Fail if more then 24 hours
    const diff = now.diff(tokenTimestampDate, 'hours');
    if (Math.abs(diff) > 24) return false;

    const userString = `${user.id}${user.email}${user.password}${user.updatedAt}`;
    const userStringHash = crypto
      .createHash('md5')
      .update(userString)
      .digest('hex');

    // Check if userInfoHash is the same - this guarantee the token used once
    return reqUserStringHash === userStringHash;
  }
  private decryptToken(stringToDecrypt: string): string {
    try {
      const key = crypto.createHash('sha256').update('popcorn').digest();

      const textParts = stringToDecrypt.split(':');
      const iv = Buffer.from(textParts.shift() as string, 'hex');
      const encryptedText = Buffer.from(textParts.join(':'), 'hex');
      const decipher = crypto.createDecipheriv('aes-256-cbc', key, iv);
      const decrypted = decipher.update(encryptedText);

      const result = Buffer.concat([decrypted, decipher.final()]);

      return result.toString();
    } catch (error) {
      console.log('decrypted token error', error);
      return null;
    }
  }

  async forgotPassword(email: string): Promise<string> {
    const user = await this.prisma.user.findUnique({ where: { email } });

    if (!user) {
      throw new NotFoundException(`No user found for email: ${email}`);
    }

    const resetToken = this.generateResetToken(user);

    this.mailerService
      .sendMail({
        to: 'test@nestjs.com', // list of receivers
        from: 'noreply@nestjs.com', // sender address
        subject: `Testing Nest MailerModule ✔ ${resetToken}`, // Subject line
        text: 'welcome', // plaintext body
        html: '<b>welcome</b>', // HTML body content
      })
      .then((success) => {
        console.log(success);
      })
      .catch((err) => {
        console.log(err);
      });

    return resetToken;
  }

  async resetPassword(payload: ResetPasswordInput): Promise<Token> {
    const decryptedToken = this.decryptToken(payload.token);
    if (!decryptedToken) {
      throw new BadRequestException('Invalid token');
    }
    const userUUID = this.getUserUUIDFromToken(decryptedToken);
    if (!userUUID) {
      throw new BadRequestException('Invalid token');
    }
    const user = await this.prisma.user.findUnique({
      where: { id: userUUID },
    });
    if (!user) {
      throw new BadRequestException('Invalid token');
    }
    const isTokenValid = await this.validateResetToken(user, decryptedToken);
    if (!isTokenValid) {
      throw new BadRequestException('Invalid token');
    }

    const hashedPassword = await this.passwordService.hashPassword(
      payload.password
    );
    try {
      await this.prisma.user.update({
        where: { id: user.id },
        data: { password: hashedPassword },
      });
    } catch (e) {
      throw new BadRequestException('Invalid token');
    }
    return this.generateTokens({
      userId: user.id,
    });
  }
}
