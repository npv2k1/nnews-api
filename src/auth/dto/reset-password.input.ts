import { IsEmail, IsNotEmpty, MinLength } from 'class-validator';
import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class ForgotPasswordInput {
  @Field()
  @IsEmail()
  email: string;
}
@InputType()
export class ResetPasswordInput {
  @Field()
  @IsNotEmpty()
  token: string;
  @Field()
  @IsNotEmpty()
  @MinLength(8)
  password: string;
}
