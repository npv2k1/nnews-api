import {
  ObjectType,
  registerEnumType,
  HideField,
  Field,
} from '@nestjs/graphql';
import { BaseModel } from 'src/common/models/base.model';
import { Role as RoleNest } from '@prisma/client';

registerEnumType(RoleNest, {
  name: 'RoleNest',
  description: 'User role',
});

@ObjectType()
export class UserNest extends BaseModel {
  email: string;
  firstname?: string;
  lastname?: string;
  @Field(() => RoleNest)
  role: RoleNest;
  @HideField()
  password: string;
}
