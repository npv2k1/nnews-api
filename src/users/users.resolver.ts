import { PrismaService } from 'nestjs-prisma';
import {
  Resolver,
  Query,
  Parent,
  Mutation,
  Args,
  ResolveField,
} from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { UserEntity } from 'src/common/decorators/user.decorator';
import { GqlAuthGuard } from 'src/auth/gql-auth.guard';
import { UsersService } from './users.service';
import { UserNest } from './models/user.model';
import { ChangePasswordInput } from './dto/change-password.input';
import { UpdateUserInput } from './dto/update-user.input';

@Resolver(() => UserNest)
@UseGuards(GqlAuthGuard)
export class UsersResolver {
  constructor(
    private usersService: UsersService,
    private prisma: PrismaService
  ) {}

  @Query(() => UserNest)
  async me(@UserEntity() user: UserNest): Promise<UserNest> {
    return user;
  }

  // @UseGuards(GqlAuthGuard)
  // @Mutation(() => User)
  // async updateUser(
  //   @UserEntity() user: User,
  //   @Args('data') newUserData: UpdateUserInput
  // ) {
  //   return this.usersService.updateUser(user.id, newUserData);
  // }

  @UseGuards(GqlAuthGuard)
  @Mutation(() => UserNest)
  async changePassword(
    @UserEntity() user: UserNest,
    @Args('data') changePassword: ChangePasswordInput
  ) {
    return this.usersService.changePassword(
      user.id,
      user.password,
      changePassword
    );
  }

  // @ResolveField('posts')
  // posts(@Parent() author: User) {
  //   return this.prisma.user.findUnique({ where: { id: author.id } }).posts();
  // }
}
