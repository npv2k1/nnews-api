import FeedParser from 'feedparser';
import got from 'got';
import { getLinkPreview, getPreviewFromContent } from 'link-preview-js';

// import fetch from 'node-fetch';

const fetchRss = async (url) => {
  const res = await got.get(url, {
    headers: {
      'user-agent':
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36',
      accept: 'text/html,application/xhtml+xml',
    },
  });
  // if (res.status !== 200) throw new Error('Bad status code');
  // if (res.)
  return res.body;
};
const parseRss = async (stream) => {
  const items = [];
  const feedparser = new FeedParser({
    addmeta: false,
  });
  await new Promise((resolve, reject) => {
    feedparser.on('error', reject);
    feedparser.on('end', resolve);
    feedparser.on('readable', function () {
      let item;
      while ((item = this.read())) {
        items.push(item);
      }
    });
    stream.pipe(feedparser);
  });
  return items;
};
const worker = async (url) => {
  try {
    const stream = await fetchRss(url);
    let items: any = await parseRss(stream);
    items = await Promise.all(
      items.map(async (item) => {
        try {
          const preview = await getLinkPreview(item.link, {
            headers: {
              'user-agent': 'googlebot',
              'Access-Control-Allow-Origin': '*',
            },
          });
          return {
            ...item,
            ...preview,
          };
        } catch (e) {
          // console.log(e);
          return item;
        }
      })
    );
    return items;
  } catch (e) {
    console.log(e);
  }
};
export default worker;
