/* eslint-disable @typescript-eslint/no-var-requires */
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

import publishers from './export/publisher.json';
import sources from './export/source.json';
import user from './export/user.json';
async function main() {
  // await prisma.user.deleteMany();
  // await prisma.post.deleteMany();

  // console.log('usersList', publishers);
  // await prisma.user.createMany({
  //   data: user,
  // });
  console.log('Seeding ');
  // await prisma.publisher.createMany({
  //   data: publishers,
  // });
  // await prisma.source.createMany({
  //   data: sources,
  // });
}

main()
  .catch((e) => console.error(e))
  .finally(async () => {
    await prisma.$disconnect();
  });
