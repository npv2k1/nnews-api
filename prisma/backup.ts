import { PrismaClient } from '@prisma/client';
import { writeFileSync } from 'fs';
import { join } from 'path';

const prisma = new PrismaClient();

async function main() {
  console.log('Backup...');
  // const publishers = await prisma.publisher.findMany();

  // const source = await prisma['source'].findMany();
  // console.log('source', source);

  Object.keys(prisma).forEach(async (key) => {
    if (key.startsWith('_')) return;
    const model = prisma[key];
    const items = await model.findMany();
    writeFileSync(
      `./prisma/export/${key}.json`,
      JSON.stringify(items, null, 2)
    );
    console.log('Exported', key);
  });
}

main()
  .catch((e) => console.error(e))
  .finally(async () => {
    await prisma.$disconnect();
  });
