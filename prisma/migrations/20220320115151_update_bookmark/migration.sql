/*
  Warnings:

  - You are about to drop the `_BookmarkToPost` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "_BookmarkToPost" DROP CONSTRAINT "_BookmarkToPost_A_fkey";

-- DropForeignKey
ALTER TABLE "_BookmarkToPost" DROP CONSTRAINT "_BookmarkToPost_B_fkey";

-- AlterTable
ALTER TABLE "Bookmark" ADD COLUMN     "postId" INTEGER;

-- DropTable
DROP TABLE "_BookmarkToPost";

-- AddForeignKey
ALTER TABLE "Bookmark" ADD CONSTRAINT "Bookmark_postId_fkey" FOREIGN KEY ("postId") REFERENCES "Post"("id") ON DELETE SET NULL ON UPDATE CASCADE;
